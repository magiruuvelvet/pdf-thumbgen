# pdf-thumbgen

```sh
# Generates PDF thumbnails for visual download buttons
#
# required files, place in script directory
#  - thumb-bg.png
#  - pdf-dl-icon-thumb.png
#
# usage: input.pdf output (.jpg automatically added)
#
```

## Requirements

 - ImageMagick

## License

Public Domain
